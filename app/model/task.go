package model

type TaskStatus string

const (
	TaskStatusPending  TaskStatus = "pending"
	TaskStatusComplete TaskStatus = "complete"
	TaskStatusProgress TaskStatus = "progress"
)

type Task struct {
	ID          uint        `gorm:"primarykey" json:"id"`
	Title       string     `gorm:"not null" json:"title"`
	Description string     `gorm:"not null" json:"description"`
	Status      TaskStatus `gorm:"not null" json:"status"`
}
