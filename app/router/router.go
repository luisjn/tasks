package router

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/luisjn/tasks/app/config"
	"gitlab.com/luisjn/tasks/app/controller"
)

func Router() *gin.Engine {
	server := gin.Default()

	server.MaxMultipartMemory = config.ENV.MaxMemory

	server.GET("/health", func(c *gin.Context) {
		c.JSON(http.StatusOK, gin.H{
			"status": http.StatusText(http.StatusOK),
		})
	})

	tasks := server.Group("/tasks")
	{
		tasks.POST("", controller.CreateTask)
		tasks.GET("", controller.ListTasks)
		tasks.GET("/:taskId", controller.GetTask)
		tasks.PUT("/:taskId", controller.UpdateTask)
		tasks.DELETE("/:taskId", controller.DeleteTask)
	}

	return server
}
