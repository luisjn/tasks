package utilstests

import (
	"net/http"
	"net/http/httptest"

	"gitlab.com/luisjn/tasks/app/router"
)

func ExecuteRequest(req *http.Request) *httptest.ResponseRecorder {
	r := router.Router()
	nr := httptest.NewRecorder()

	r.ServeHTTP(nr, req)

	return nr
}
