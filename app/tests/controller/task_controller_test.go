package controller_test

import (
	"bytes"
	"fmt"
	"net/http"
	"net/http/httptest"

	"gitlab.com/luisjn/tasks/app/db"
	"gitlab.com/luisjn/tasks/app/model"
	utilstests "gitlab.com/luisjn/tasks/app/tests/utils"

	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"
)

var _ = Describe("TaskController", func() {
	utilstests.LoadEnv()
	db.Connect()

	var task *model.Task

	BeforeEach(func() {
		if err := db.CONN.Exec("DELETE FROM tasks;").Error; err != nil {
			panic(fmt.Errorf("failed to delete tasks. %w", err))
		}

		task = &model.Task{
			Title:       "Task 1",
			Description: "Description 1",
			Status:      model.TaskStatusPending,
		}

		db.CONN.Create(task)
	})

	AfterEach(func() {
		if err := db.CONN.Exec("DELETE FROM tasks;").Error; err != nil {
			panic(fmt.Errorf("failed to delete tasks. %w", err))
		}
	})

	Describe("ListTasks", func() {
		var response *httptest.ResponseRecorder

		JustBeforeEach(func() {
			req, _ := http.NewRequest(http.MethodGet, "/tasks", nil)
			response = utilstests.ExecuteRequest(req)
		})

		Context("should list tasks", func() {
			It("should return status 200", func() {
				Expect(response.Code).To(Equal(http.StatusOK))
			})

			It("body should not be nil", func() {
				Expect(response.Body).ToNot(BeNil())
			})

			It("body should have same values", func() {
				l, _ := utilstests.DeserializeList(response.Body.String())
				Expect(len(l)).To(Equal(1))
				for _, t := range l {
					Expect(t["id"]).To(BeEquivalentTo(task.ID))
					Expect(t["title"]).To(Equal("Task 1"))
					Expect(t["description"]).To(Equal("Description 1"))
					Expect(t["status"]).To(Equal("pending"))
				}
			})
		})
	})

	Describe("GetTask", func() {
		var response *httptest.ResponseRecorder

		JustBeforeEach(func() {
			req, _ := http.NewRequest(http.MethodGet, "/tasks/"+fmt.Sprintf("%v", task.ID), nil)
			response = utilstests.ExecuteRequest(req)
		})

		Context("should get task", func() {
			It("should return status 200", func() {
				Expect(response.Code).To(Equal(http.StatusOK))
			})

			It("body should not be nil", func() {
				Expect(response.Body).ToNot(BeNil())
			})

			It("body should have same values", func() {
				t, _ := utilstests.Deserialize(response.Body.String())
				Expect(t["id"]).To(BeEquivalentTo(task.ID))
				Expect(t["title"]).To(Equal("Task 1"))
				Expect(t["description"]).To(Equal("Description 1"))
				Expect(t["status"]).To(Equal("pending"))
			})
		})

		Context("should get 404 response", func() {
			JustBeforeEach(func() {
				req, _ := http.NewRequest(http.MethodGet, "/tasks/0", nil)
				response = utilstests.ExecuteRequest(req)
			})

			It("should return status 404", func() {
				Expect(response.Code).To(Equal(404))
			})

			It("body should not be nil", func() {
				Expect(response.Body).ToNot(BeNil())
			})
		})
	})

	Describe("CreateTask", func() {
		var response *httptest.ResponseRecorder
		var payload []byte

		BeforeEach(func() {
			payload = []byte(`{
				"title": "Task 2",
				"description": "Description 2",
				"status": "pending"
			}`)
		})

		JustBeforeEach(func() {
			req, _ := http.NewRequest(http.MethodPost, "/tasks", bytes.NewBuffer(payload))
			response = utilstests.ExecuteRequest(req)
		})

		Context("should create a task", func() {
			It("should return status 201", func() {
				Expect(response.Code).To(Equal(http.StatusCreated))
			})

			It("body should not be nil", func() {
				Expect(response.Body).ToNot(BeNil())
			})

			It("body should have same values", func() {
				t, _ := utilstests.Deserialize(response.Body.String())
				Expect(t["id"]).ToNot(BeNil())
				Expect(t["title"]).To(Equal("Task 2"))
				Expect(t["description"]).To(Equal("Description 2"))
				Expect(t["status"]).To(Equal("pending"))
			})
		})

		Context("should get error when create a task without title", func() {
			BeforeEach(func() {
				payload = []byte(`{
					"description": "Description 2",
					"status": "pending"
				}`)
			})

			It("should return status 400", func() {
				Expect(response.Code).To(Equal(400))
			})

			It("body should not be nil", func() {
				Expect(response.Body).ToNot(BeNil())
			})
		})
	})

	Describe("UpdateTask", func() {
		var response *httptest.ResponseRecorder
		var payload []byte
		var payloadID uint

		BeforeEach(func() {
			payloadID = task.ID
			payload = []byte(`{
				"title": "Task 3",
				"description": "Description 3",
				"status": "pending"
			}`)
		})

		JustBeforeEach(func() {
			req, _ := http.NewRequest(http.MethodPut, "/tasks/"+fmt.Sprintf("%v", payloadID), bytes.NewBuffer(payload))
			response = utilstests.ExecuteRequest(req)
		})

		Context("should update a task", func() {
			It("should return status 200", func() {
				Expect(response.Code).To(Equal(http.StatusOK))
			})

			It("body should not be nil", func() {
				Expect(response.Body).ToNot(BeNil())
			})

			It("body should have same values", func() {
				t, _ := utilstests.Deserialize(response.Body.String())
				Expect(t["id"]).To(BeEquivalentTo(1))
				Expect(t["title"]).To(Equal("Task 3"))
				Expect(t["description"]).To(Equal("Description 3"))
				Expect(t["status"]).To(Equal("pending"))
			})
		})

		Context("should get error when update a task without title", func() {
			BeforeEach(func() {
				payloadID = task.ID
				payload = []byte(`{
					"description": "Description 3",
					"status": "pending"
				}`)
			})

			It("should return status 400", func() {
				Expect(response.Code).To(Equal(400))
			})

			It("body should not be nil", func() {
				Expect(response.Body).ToNot(BeNil())
			})
		})
	})

	Describe("DeleteTask", func() {
		var response *httptest.ResponseRecorder
		var ntask *model.Task

		BeforeEach(func() {
			ntask = &model.Task{
				Title:       "Task 1000",
				Description: "Description 1000",
				Status:      model.TaskStatusPending,
			}

			db.CONN.Create(ntask)
		})

		JustBeforeEach(func() {
			req, _ := http.NewRequest(http.MethodDelete, "/tasks/"+fmt.Sprintf("%v", ntask.ID), nil)
			response = utilstests.ExecuteRequest(req)
		})

		Context("should delete a task", func() {
			It("should return status 204", func() {
				Expect(response.Code).To(Equal(http.StatusNoContent))
			})
		})

		Context("should get 404 response", func() {
			JustBeforeEach(func() {
				req, _ := http.NewRequest(http.MethodDelete, "/tasks/1000", nil)
				response = utilstests.ExecuteRequest(req)
			})

			It("should return status 404", func() {
				Expect(response.Code).To(Equal(404))
			})

			It("body should not be nil", func() {
				Expect(response.Body).ToNot(BeNil())
			})
		})
	})

})
