package main

import (
	"fmt"
	"log"

	"gitlab.com/luisjn/tasks/app/config"
	"gitlab.com/luisjn/tasks/app/db"
	"gitlab.com/luisjn/tasks/app/router"
)

func main() {
	config.Load()
	db.Connect()

	server := router.Router()
	port := fmt.Sprintf(":%s", config.ENV.Port)

	if err := server.Run(port); err != nil {
		log.Fatalln("error when server is initializing")
	}
}
