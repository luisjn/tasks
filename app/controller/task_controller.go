package controller

import (
	"log"
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/luisjn/tasks/app/db"
	"gitlab.com/luisjn/tasks/app/model"
	"gorm.io/gorm"
)

func ListTasks(context *gin.Context) {
	var tasks []model.Task

	var result *gorm.DB

	if result = db.CONN.Find(&tasks); result.Error != nil {
		context.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"error": result.Error.Error()})
		return
	}

	log.Printf("Found %v tasks", len(tasks))
	context.JSON(http.StatusOK, &tasks)
}

func GetTask(context *gin.Context) {
	taskID := context.Param("taskId")

	var task model.Task
	if result := db.CONN.First(&task, taskID); result.Error != nil {
		context.AbortWithStatusJSON(http.StatusNotFound, gin.H{"error": result.Error.Error()})
		return
	}

	context.JSON(http.StatusOK, &task)
}

func CreateTask(context *gin.Context) {
	var taskRequest struct {
		Title       string           `json:"title" binding:"required,gte=1,lte=255"`
		Description string           `json:"description" binding:"required,gte=1,lte=255"`
		Status      model.TaskStatus `json:"status" binding:"required,oneof=pending complete progress"`
	}

	if err := context.BindJSON(&taskRequest); err != nil {
		context.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	task := model.Task{
		Title:       taskRequest.Title,
		Description: taskRequest.Description,
		Status:      taskRequest.Status,
	}

	if result := db.CONN.Create(&task); result.Error != nil {
		context.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": result.Error})
		return
	}

	log.Printf("task %v has been created", task.ID)
	context.JSON(http.StatusCreated, &task)
}

func DeleteTask(context *gin.Context) {
	taskID := context.Param("taskId")

	var task model.Task

	if result := db.CONN.First(&task, taskID); result.Error != nil {
		context.AbortWithStatusJSON(http.StatusNotFound, gin.H{"error": result.Error.Error()})
		return
	}

	if result := db.CONN.Delete(&model.Task{}, taskID); result.Error != nil {
		context.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": result.Error})
		return
	}

	context.Status(http.StatusNoContent)
}

func UpdateTask(context *gin.Context) {
	taskID := context.Param("taskId")

	var taskRequest struct {
		Title       string           `json:"title" binding:"required,gte=1,lte=255"`
		Description string           `json:"description" binding:"required,gte=1,lte=255"`
		Status      model.TaskStatus `json:"status" binding:"required,oneof=pending complete progress"`
	}

	if err := context.BindJSON(&taskRequest); err != nil {
		context.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	var task model.Task

	if result := db.CONN.First(&task, taskID); result.Error != nil {
		context.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"error": result.Error.Error()})
		return
	}

	task.Title = taskRequest.Title
	task.Description = taskRequest.Description
	task.Status = taskRequest.Status

	if result := db.CONN.Save(&task); result.Error != nil {
		context.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": result.Error})
		return
	}

	context.JSON(http.StatusOK, &task)
}
