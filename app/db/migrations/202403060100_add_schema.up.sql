CREATE TABLE `tasks` (
  `id` integer,
  `title` text NOT NULL,
  `description` text NOT NULL,
  `status` text NOT NULL,
  PRIMARY KEY (`id`)
);