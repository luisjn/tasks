package cmd

import (
	"log"

	"gitlab.com/luisjn/tasks/app/db"

	"github.com/spf13/cobra"
)

var migrateCmd = &cobra.Command{
	Use:   "migrate",
	Short: "Run migrations of database",
	Run: func(cmd *cobra.Command, args []string) {
		if len(args) > 0 {
			switch args[0] {
			case "up":
				log.Println("migration upping...")
				db.Up()
			case "down":
				log.Println("migration downing...")
				db.Down()
			}
		}
	},
}

func init() {
	rootCmd.AddCommand(migrateCmd)
}
