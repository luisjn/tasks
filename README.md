# Tasks API

This is an API Restful app written in Go to manage simple tasks.

## Project Setup

1. Clone repository on your terminal
    ```
    git clone https://gitlab.com/luisjn/tasks.git
    ```

2. Prepare the app
    ```
    make prepare
    ```

3. Run the tests
    ```
    make test
    ```

3. Run the app
    ```
    make start
    ```

## Endpoints
#### Creating new task
<code>POST</code> <code><b>/tasks</b></code>
#### Listing existing tasks
<code>GET</code> <code><b>/tasks</b></code>

<code>GET</code> <code><b>/tasks/{taskId}</b></code>
#### Updating existing tasks
<code>PUT</code> <code><b>/tasks/{taskId}</b></code>
#### Deleting existing tasks
<code>DELETE</code> <code><b>/tasks/{taskId}</b></code>
### Task Object
```
{
    "id": 1
    "title": "Task",
    "description": "Description",
    "status": "pending"
}
 ```

### Task Status

- complete
- pending
- progress