migrate-up:
	go run cli.go migrate up

migrate-down:
	go run cli.go migrate down

migrate-test-up:
	GO_ENVIRONMENT=test go run cli.go migrate up

migrate-test-down:
	GO_ENVIRONMENT=test go run cli.go migrate down

migrate-up:
	go run cli.go migrate up

migrate-down:
	go run cli.go migrate down

start:
	go run app/main.go

lint:
	golangci-lint run ./...

install:
	go get ./...

prepare: install migrate-up

test:
	go test ./app/tests/...